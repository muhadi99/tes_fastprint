<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Fastprint extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    function data_post() {
        $username = $this->post('username');
        $password = md5($this->post('password'));

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://recruitment.fastprint.co.id/tes/api_tes_programmer',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => array('username' => $username,'password' => $password),
          CURLOPT_HTTPHEADER => array(
            'Cookie: ci_session=hb3ch4iuo0ig6636gi390s8as2nmv22l'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;

        $note = json_decode($response, true);
        // echo $note['error'];
        // echo $note['ket'];

        if ($note['error'] == 1) {
            $this->session->set_flashdata('gagal',$note['ket']);
            
        } else {
            $data = json_decode($response,true);
            foreach ($data['data'] as $row) {
                $no = $row['no'];
                $cek = $this->db->query("SELECT no FROM produk WHERE no = $no ")->num_rows();
                if ($cek > 0) {
                    $this->session->set_flashdata('gagal','Data sudah berhasil diambil sebelumnya!');
                } else {
                    $data = array(
                        'no' => $row['no'],
                        'id_produk' => $row['id_produk'],
                        'nama_produk' => $row['nama_produk'],
                        'kategori' => $row['kategori'],
                        'harga' => $row['harga'],
                        'status' => $row['status'],
                    );
                    $this->db->insert('produk', $data);
                    $this->session->set_flashdata('sukses','Data Berhasil Diambil');
                }
                
            }
        }

        redirect('');
        
    }


}