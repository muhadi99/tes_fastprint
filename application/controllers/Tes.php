<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tes extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('ambildataapi');
	}

	public function seluruhdata()
	{
		$data['data'] = $this->db->query("SELECT * FROM produk ORDER BY created_at DESC")->result();
		$this->load->view('seluruhdata',$data);
	}

	public function databisadijual()
	{
		$data['data'] = $this->db->query("SELECT * FROM produk WHERE status = 'bisa dijual' ORDER BY created_at DESC")->result();
		$this->load->view('databisadijual',$data);
	}

	function tambah_produk() 
	{
		$nama_produk = $this->input->post('nama_produk');
		$kategori = $this->input->post('kategori');
		$harga = $this->input->post('harga');
		$status = $this->input->post('status');

		$halaman = $this->input->post('halaman');

        $data = array(
        	'nama_produk' => $nama_produk,
        	'kategori' => $kategori,
        	'harga' => $harga,
        	'status' => $status,
        );
        $this->db->insert('produk', $data);
        $this->session->set_flashdata('sukses','Data Produk dgn nama '.$nama_produk.' Berhasil Ditambahkan');

        if ($halaman == "seluruhdata") {
        	redirect('tes/seluruhdata');
        } else {
        	redirect('tes/databisadijual');
        }

        
    }

	function edit_produk() 
	{
		$id_produk = $this->input->post('id_produk');
		$nama_produk = $this->input->post('nama_produk');
		$kategori = $this->input->post('kategori');
		$harga = $this->input->post('harga');
		$status = $this->input->post('status');

		$halaman = $this->input->post('halaman');

        $data = array(
        	'nama_produk' => $nama_produk,
        	'kategori' => $kategori,
        	'harga' => $harga,
        	'status' => $status,
        );
        $this->db->where('id_produk', $id_produk);
        $this->db->update('produk', $data);
        $this->session->set_flashdata('sukses','Data Produk Berhasil Diperbarui');

        if ($halaman == "seluruhdata") {
        	redirect('tes/seluruhdata');
        } else {
        	redirect('tes/databisadijual');
        }
    }

    function hapus_produk() 
	{
        $nama_produk = $this->input->post('nama_produk');
        $id_produk = $this->input->post('id_produk');

        $hapus = $this->db->query("DELETE FROM produk WHERE id_produk = $id_produk ");
        $this->session->set_flashdata('sukses','Produk dgn nama '.$nama_produk.' Berhasil Dihapus!');
    }

    public function kosongkan()
	{
		$this->load->view('kosongkan');
	}

	function kosong() 
	{

        $hapus = $this->db->query("TRUNCATE produk ");

        $this->session->set_flashdata('sukses','Data Berhasil Dikosongkan!');
    }
}
