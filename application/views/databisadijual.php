<!DOCTYPE html>
<html>

<?php $this->load->view("layout/head.php"); ?>

<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">


<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view("layout/header.php"); ?>

  <?php $this->load->view("layout/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

      <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah Data</h4>
            </div>
            <div class="modal-body">
              <div class="box-body">
                <form action="<?= base_url() ?>tes/tambah_produk" method="post" class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Nama Produk</label>
                      <input class="form-control" type="text" name="nama_produk" required>
                    </div>
                    <div class="form-group">
                      <label>Kategori</label>
                      <input class="form-control" type="text" name="kategori" required>
                    </div>
                    <div class="form-group">
                      <label>Harga</label>
                      <input class="form-control" type="number" name="harga" required>
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <input class="form-control" type="text" name="status" required>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit Data</h4>
            </div>
            <div class="modal-body">
              <div class="box-body">
                <form action="<?= base_url() ?>tes/edit_produk" method="post" class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Nama Produk</label>
                      <input type="hidden" id="id_produk" name="id_produk">
                      <input class="form-control" type="text" name="nama_produk" id="nama_produk" required>
                    </div>
                    <div class="form-group">
                      <label>Kategori</label>
                      <input class="form-control" type="text" name="kategori" id="kategori" required>
                    </div>
                    <div class="form-group">
                      <label>Harga</label>
                      <input class="form-control" type="number" name="harga" id="harga" required>
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <input class="form-control" type="text" name="status" id="status" required>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-warning"><i class="fa fa-pencil"></i> Simpan Perubahan</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Seluruh Data</h3>
          <div class="box-tools pull-right">
            <a data-toggle="modal" data-target="#modal-tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Produk</a>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="alert alert-success alert-dismissible" <?php if($this->session->flashdata('sukses') != "") echo ''; else echo 'hidden'; ?>>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
              <?= $this->session->flashdata('sukses') ?>
            </div>
            <div class="alert alert-danger alert-dismissible" <?php if($this->session->flashdata('gagal') != "") echo ''; else echo 'hidden'; ?>>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Gagal!</h4>
              <?= $this->session->flashdata('gagal') ?>
            </div>
            <div class="box-body" style="overflow-x: auto;">
              <table class="table table-stripped table-hover table-bordered datatables text-center">
                <thead>
                  <th width="5px">No</th>
                  <th>Nama Produk</th>
                  <th>Kategori</th>
                  <th>Harga</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                  <?php
                  $no=0;
                  foreach ($data as $row) { $no++;?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $row->nama_produk ?></td>
                      <td><?= $row->kategori ?></td>
                      <td>Rp.<?= number_format($row->harga) ?></td>
                      <td><?= $row->status ?></td>
                      <td>
                        <a data-toggle="modal" data-target="#modal-edit" data-id_produk="<?= $row->id_produk ?>" data-nama_produk="<?= $row->nama_produk ?>" data-kategori="<?= $row->kategori ?>" data-harga="<?= $row->harga ?>" data-status="<?= $row->status ?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                        <a data-id_produk='<?= $row->id_produk ?>' data-nama_produk='<?= $row->nama_produk ?>' class='btn btn-sm btn-danger hapus'><i class='fa fa-trash-o'></i> Hapus</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
              <!-- /.box-body -->
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view("layout/footer.php"); ?>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view("layout/js.php"); ?>


<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- SweetAlert -->
<script src="<?php echo base_url(); ?>assets/backend/sweetalert.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    $('#modal-edit').on('show.bs.modal', function (event) {
        $(this).find('form').trigger('reset');
        var div = $(event.relatedTarget)
        var modal          = $(this)

        modal.find('#id_produk').attr("value",div.data('id_produk'));
        modal.find('#nama_produk').attr("value",div.data('nama_produk'));
        modal.find('#kategori').attr("value",div.data('kategori'));
        modal.find('#harga').attr("value",div.data('harga'));
        modal.find('#status').attr("value",div.data('status'));
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {


    $(".hapus").click(function()
    {
      var id_produk = $(this).data('id_produk');
      var nama_produk = $(this).data('nama_produk');
      
      swal({
        title: "Apa anda yakin menghapus data produk dgn nama "+ nama_produk +" ?",
        icon: "warning",
        buttons: ["No", "Yes"],
        dangerMode: true,
      }).
      then((ok) => {
        if (ok) 
        {
          $.ajax({
              url: "<?=base_url('tes/hapus_produk')?>",
              dataType: 'text',
              type: 'post',
               data: {id_produk:id_produk,nama_produk:nama_produk},
               success: function (data) {
                  swal("Berhasil", "Klik OK Untuk Melanjutkan!", "success")
                  .then((value) => {
                    location.reload(true);
                  });
                }
              });
        } else {
          swal("Proses Dibatalkan!");
        }
      });
      
    });

  });
</script>

<script>
  $(document).ready(function() {
    $(function () {
      $('.datatables').dataTable({
      destroy: true,
      paging: true,
      searching: true,
      info: true,
      ordering: true,
      });
    });
  });
</script>

</body>
</html>
