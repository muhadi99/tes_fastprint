<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url() ?>assets/muhadi.jpeg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Muhadi</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>

      <li class="<?php if(($this->uri->segment(2)=="")) { echo "active"; } ?>"><a href="<?= base_url() ?>"><i class="fa fa-check"></i> <span>Ambil Data API</span></a></li>
      <li class="<?php if(($this->uri->segment(2)=="seluruhdata")) { echo "active"; } ?>"><a href="<?= base_url() ?>tes/seluruhdata"><i class="fa fa-check"></i> <span>Seluruh Data</span></a></li>
      <li class="<?php if(($this->uri->segment(2)=="databisadijual")) { echo "active"; } ?>"><a href="<?= base_url() ?>tes/databisadijual"><i class="fa fa-check"></i> <span>Data Bisa Dijual</span></a></li>
      <li class="<?php if(($this->uri->segment(2)=="kosongkan")) { echo "active"; } ?>"><a href="<?= base_url() ?>tes/kosongkan"><i class="fa fa-check"></i> <span>Kosongkan Data</span></a></li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>