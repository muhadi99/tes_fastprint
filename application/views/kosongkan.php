<!DOCTYPE html>
<html>

<?php $this->load->view("layout/head.php"); ?>

<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">


<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view("layout/header.php"); ?>

  <?php $this->load->view("layout/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <div style="text-align: center;">
            <a class="btn btn-danger kosong"><i class="fa fa-warning"></i> Kosongkan Data</a>
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view("layout/footer.php"); ?>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view("layout/js.php"); ?>


<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- SweetAlert -->
<script src="<?php echo base_url(); ?>assets/backend/sweetalert.min.js"></script>


<script type="text/javascript">
  $(document).ready(function() {


    $(".kosong").click(function()
    {
      var id = 1;
      
      swal({
        title: "Apa anda yakin mengkosongkan seluruh data ?",
        icon: "warning",
        buttons: ["No", "Yes"],
        dangerMode: true,
      }).
      then((ok) => {
        if (ok) 
        {
          $.ajax({
              url: "<?=base_url('tes/kosong')?>",
              dataType: 'text',
              type: 'post',
               data: {id:id},
               success: function (data) {
                  swal("Berhasil", "Klik OK Untuk Melanjutkan!", "success")
                  .then((value) => {
                    location.reload(true);
                  });
                }
              });
        } else {
          swal("Proses Dibatalkan!");
        }
      });
      
    });

  });
</script>

<script>
  $(document).ready(function() {
    $(function () {
      $('.datatables').dataTable({
      destroy: true,
      paging: true,
      searching: true,
      info: true,
      ordering: true,
      });
    });
  });
</script>

</body>
</html>
