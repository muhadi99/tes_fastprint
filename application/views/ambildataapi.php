<!DOCTYPE html>
<html>

<?php $this->load->view("layout/head.php"); ?>

<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">


<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view("layout/header.php"); ?>

  <?php $this->load->view("layout/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ambil Data API</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="alert alert-success alert-dismissible" <?php if($this->session->flashdata('sukses') != "") echo ''; else echo 'hidden'; ?>>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
              <?= $this->session->flashdata('sukses') ?>
            </div>
            <div class="alert alert-danger alert-dismissible" <?php if($this->session->flashdata('gagal') != "") echo ''; else echo 'hidden'; ?>>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Gagal!</h4>
              <?= $this->session->flashdata('gagal') ?>
            </div>
            <div class="box-body" style="overflow-x: auto;">
              <form action="<?= base_url() ?>fastprint/data" method="post" class="form-horizontal">
                <div class="box-body">
                  <div class="form-group">
                    <label>Username</label>
                    <input class="form-control" type="text" name="username" required>
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="text" name="password" required>
                  </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                  <button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
                </div>
              </form>
            </div>
              <!-- /.box-body -->
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view("layout/footer.php"); ?>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view("layout/js.php"); ?>


<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- SweetAlert -->
<script src="<?php echo base_url(); ?>assets/backend/sweetalert.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    $('#modal-edit').on('show.bs.modal', function (event) {
        $(this).find('form').trigger('reset');
        var div = $(event.relatedTarget)
        var modal          = $(this)

        modal.find('#id').attr("value",div.data('id'));
        modal.find('#nama_kategori').attr("value",div.data('nama_kategori'));
        modal.find('#kode_kategori').attr("value",div.data('kode_kategori'));
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {


    $(".hapus").click(function()
    {
      var id = $(this).data('id');
      var nama_kategori = $(this).data('nama_kategori');
      
      swal({
        title: "Apa anda yakin menghapus data kategori barang a.n "+ nama_kategori +" ?",
        icon: "warning",
        buttons: ["No", "Yes"],
        dangerMode: true,
      }).
      then((ok) => {
        if (ok) 
        {
          $.ajax({
              url: "<?=base_url('admin/hapus_kategori')?>",
              dataType: 'text',
              type: 'post',
               data: {id:id,nama_kategori:nama_kategori},
               success: function (data) {
                  swal("Berhasil", "Klik OK Untuk Melanjutkan!", "success")
                  .then((value) => {
                    location.reload(true);
                  });
                }
              });
        } else {
          swal("Proses Dibatalkan!");
        }
      });
      
    });

  });
</script>

<script>
  $(document).ready(function() {
    $(function () {
      $('.datatables').dataTable({
      destroy: true,
      paging: true,
      searching: true,
      info: true,
      ordering: true,
      });
    });
  });
</script>

</body>
</html>
